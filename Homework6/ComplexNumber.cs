﻿using System;

namespace Homework6
{
    public class ComplexNumber : ICloneable

    {
        
    public double Real { get; set; }
    public double Imaginary { get; set; }

    public ComplexNumber()
    {
        Real = 0;
        Imaginary = 0;
    }

    public ComplexNumber(double real, double imaginary)
    {
        Real = real;
        Imaginary = imaginary;
    }

    public ComplexNumber add(ComplexNumber b)
    {
        var SpaceNumber = new ComplexNumber(Real += b.Real, Imaginary += b.Imaginary);
        return SpaceNumber;
    }

    public void add2(ComplexNumber b)
    {
        var NewComplexNumber = add(b);
        (Real, Imaginary) = (NewComplexNumber.Real, NewComplexNumber.Imaginary);
    }

    public ComplexNumber sub(ComplexNumber b)
    {
        var SpaceNumber = new ComplexNumber(Real -= b.Real, Imaginary -= b.Imaginary);
        return SpaceNumber;
    }

    public void sub2(ComplexNumber b)
    {
        var NewComplexNumber = add(b);
        (Real, Imaginary) = (NewComplexNumber.Real, NewComplexNumber.Imaginary);
    }

    public ComplexNumber MultNumber(double b)
    {
        var SpaceNumber = new ComplexNumber(Real *= b, Imaginary *= b);
        return SpaceNumber;
    }

    public void MultNumber2(double b)
    {
        var NewComplexNumber = MultNumber(b);
        (Real, Imaginary) = (NewComplexNumber.Real, NewComplexNumber.Imaginary);
    }

    public ComplexNumber Mult(ComplexNumber b)
    {
        var SpaceNumber = new ComplexNumber(Real = (Real * b.Real - Imaginary * b.Imaginary),
            Imaginary = Real * b.Imaginary + Imaginary * b.Real);
        return SpaceNumber;
    }

    public void Mult2(ComplexNumber b)
    {
        var NewComplexNumber = Mult(b);
        (Real, Imaginary) = (NewComplexNumber.Real, NewComplexNumber.Imaginary);
    }

    public ComplexNumber Div(ComplexNumber b)
    {
        var SpaceNumber = new ComplexNumber(
            Real = ((Real * b.Real - Imaginary * b.Imaginary) / (b.Real * b.Real +
                                                                 b.Imaginary * b.Imaginary)),
            Imaginary = (Real * b.Imaginary + Imaginary * b.Real) / (b.Real * b.Real +
                                                                     b.Imaginary * b.Imaginary));
        return SpaceNumber;
    }

    public void Div2(ComplexNumber b)
    {
        var NewComplexNumber = Div(b);
        (Real, Imaginary) = (NewComplexNumber.Real, NewComplexNumber.Imaginary);
    }

    public double Length()
    {
        var length = Real * Real + Imaginary * Imaginary;
        return length;
    }

    public string toString()
    {
        if (Imaginary > 0)
        {
            return $"{Real}+{Imaginary}*i";
        }
        else
        {
            return $"{Real}-{Imaginary}*i";
        }
    }

    public double arg()
    {
        var ard = Math.Atan(Imaginary / Real);
        return ard;
    }

    public ComplexNumber pow(double b)
    {
        var length = Math.Pow(Length(), b);
        var NewComplexNumber = new ComplexNumber(Real = length * Math.Cos(arg() * b),
            Imaginary = length * Math.Sin(arg() * b));
        return NewComplexNumber;
    }

    public bool eauls(ComplexNumber b)
    {
        return (Real == b.Real && Imaginary == b.Imaginary);
    }


    object ICloneable.Clone()
    {
        return new ComplexNumber(Real,Imaginary);
    }
    }
}