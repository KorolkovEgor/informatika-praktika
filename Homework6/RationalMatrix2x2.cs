﻿using System;

namespace Homework6
{
    public class Matrix2x2
    {
        private double[,] matrix = new double[2, 2];
        public double[,] Matrix { get { return matrix; } }
        public Matrix2x2()
        {
            matrix[0, 0] = 0;
            matrix[0, 1] = 0;
            matrix[1, 0] = 0;
            matrix[1, 1] = 0;
        }
        public Matrix2x2(double x)
        {
            matrix[0, 0] = x;
            matrix[0, 1] = x;
            matrix[1, 0] = x;
            matrix[1, 1] = x;
        }
        public Matrix2x2(double[,] x)
        {
            matrix = x;
        }

        public Matrix2x2(double x1, double x2, double x3, double x4)
        {
            matrix[0, 0] = x1;
            matrix[0, 1] = x2;
            matrix[1, 0] = x3;
            matrix[1, 1] = x4;
        }

        public Matrix2x2 Add(Matrix2x2 m)
        {
            return new Matrix2x2(matrix[0, 0] + m.matrix[0, 0], matrix[0, 1] + m.matrix[0, 1],
                matrix[1, 0] + m.matrix[1, 0], matrix[1, 1] + m.matrix[1, 1]);
        }
        public void Add_ToYourself(Matrix2x2 m)
        {
            matrix[0, 0] = matrix[0, 0] + m.matrix[0, 0];
            matrix[0, 1] = matrix[0, 1] + m.matrix[0, 1];
            matrix[1, 0] = matrix[1, 0] + m.matrix[1, 0];
            matrix[1, 1] = matrix[1, 1] + m.matrix[1, 1];
        }
        public Matrix2x2 Sub(Matrix2x2 m)
        {
            return new Matrix2x2(matrix[0, 0] - m.matrix[0, 0], matrix[0, 1] - m.matrix[0, 1],
                matrix[1, 0] - m.matrix[1, 0], matrix[1, 1] - m.matrix[1, 1]);
        }
        public void Sub_ToYourself(Matrix2x2 m)
        {
            matrix[0, 0] = matrix[0, 0] - m.matrix[0, 0];
            matrix[0, 1] = matrix[0, 1] - m.matrix[0, 1];
            matrix[1, 0] = matrix[1, 0] - m.matrix[1, 0];
            matrix[1, 1] = matrix[1, 1] - m.matrix[1, 1];
        }
        public Matrix2x2 MultNumber(double x)
        {
            return new Matrix2x2(matrix[0, 0]*x, matrix[0, 1]*x,
                            matrix[1, 0]*x, matrix[1, 1]*x);
        }
        public Matrix2x2 Mult(Matrix2x2 m)
        {
            return new Matrix2x2(matrix[0, 0] * m.matrix[0, 0], matrix[0, 1] * m.matrix[0, 1],
                matrix[1, 0] * m.matrix[1, 0], matrix[1, 1] * m.matrix[1, 1]);
        }
        public void Mult_ToYourself(Matrix2x2 m)
        {
            matrix[0, 0] = matrix[0, 0] * m.matrix[0, 0];
            matrix[0, 1] = matrix[0, 1] * m.matrix[0, 1];
            matrix[1, 0] = matrix[1, 0] * m.matrix[1, 0];
            matrix[1, 1] = matrix[1, 1] * m.matrix[1, 1];
        }
        public double Det()
        {
            return (matrix[0, 0] * matrix[1, 1]) - (matrix[0, 1] * matrix[1, 0]);
        }
        public void Transpon()
        {
            double x = matrix[0, 1];
            matrix[0, 1] = matrix[1, 0];
            matrix[1, 0] = x;
        }
        public Matrix2x2 InverseMatrix()
        {
            double det = this.Det();
            if (det == 0)
                return new Matrix2x2();
            return new Matrix2x2(matrix[1, 1] / det, (-1) * matrix[0, 1] / det,
                            (-1) * matrix[1, 0] / det, matrix[0, 0] / det);
        }
        public Matrix2x2 EquivalentDiagonal()
        {
            if (matrix[0, 0]==0 || matrix[1, 1] == 0)
                return new Matrix2x2();
            double k1 = matrix[1, 0] / matrix[0, 0];
            matrix[1, 0] = 0;
            matrix[1, 1] = matrix[1, 1] + (-1) * matrix[0, 1] * k1;
            double k2 = matrix[0, 1] / matrix[1, 1];
            matrix[0, 1] = 0;
            return new Matrix2x2(matrix[0, 0], matrix[0, 1], matrix[1, 0], matrix[1, 1]);
        }
        public Vector MultVector(Vector vector)
        {
            return new Vector(vector.X * matrix[0, 0] * matrix[0, 1],
                              vector.Y * matrix[1, 0] * matrix[1, 1]);
        }
    }
    public class Vector
    {
        public double X { get; set; }
        public double Y { get; set; }
        public Vector() { X = 0; Y = 0; }
        public Vector(double x_coordinate, double y_coordinate)
        {
            X = x_coordinate;
            Y = y_coordinate;
        }
        public Vector Add(Vector vector)
        {
            return new Vector(X + vector.X, Y + vector.Y);
        }
        public void Add_to_Yourself(Vector vector)
        {
            X += vector.X;
            Y += vector.Y;
        }
        public Vector Sub(Vector vector)
        {
            return new Vector(X + vector.X, Y + vector.Y);
        }
        public void Sub_to_Yourself(Vector vector)
        {
            X -= vector.X;
            Y -= vector.Y;
        }
        public Vector Mult(double coeff)
        {
            return new Vector(X * coeff, Y * coeff);
        }
        public void Mult_to_Yourself(double coeff)
        {
            X *= coeff;
            Y *= coeff;

        }
        public void Vector_ToString()
        {
            Console.WriteLine($"Vector({X};{Y})");
        }
        public double Vector_Length()
        {
            double length = Math.Sqrt(X * X + Y * Y);
            return length;
        }
        public double Vector_Scalar_Mult(Vector vector)
        {
            double scalar_mult = vector.X * X + vector.Y * Y;
            return scalar_mult;
        }
        public double Cosinus(Vector vector)
        {
            double cosinus = Vector_Scalar_Mult(vector) / Vector_Length() * vector.Vector_Length();
            return cosinus;
        }
        public bool Equality(Vector vector)
        {
            return X == vector.X && Y == vector.Y;
        }
    }
}