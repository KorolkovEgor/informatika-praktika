﻿namespace Homework6
{
    public class ComplexMatrix
    {
        private ComplexNumber[,] array = new ComplexNumber[2, 2];

        public ComplexMatrix()
        {
            ComplexNumber n = new ComplexNumber(0, 0);
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 0; j++)
                {
                    array[i, j] = n;
                }
            }
        }
        public ComplexMatrix(ComplexNumber n)
        {
            for (int i = 0; i <2 ; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    array[i, j] = n;
                }
            }
        }

        public ComplexMatrix(ComplexNumber a, ComplexNumber b, ComplexNumber c, ComplexNumber d)
        {
            array[0, 0] = a;
            array[0, 1] = b;
            array[1, 0] = c;
            array[1, 1] = d;
        }

        public ComplexMatrix Add(ComplexMatrix r)
        {
            ComplexNumber a = r.array[0, 0], d = r.array[1, 1], c = r.array[1, 0], b = r.array[0, 1];
            a.add2(array[0, 0]);
            b.add2(array[0,1]);
            c.add2(array[1,0]);
            d.add2(array[1,1]);
            ComplexMatrix newA = new ComplexMatrix(a,b,c,d);
            return newA;
        }
        public ComplexMatrix Mult(ComplexMatrix r)
        {
            ComplexNumber a = new ComplexNumber(), b = new ComplexNumber(), c = new ComplexNumber(), d = new ComplexNumber();
            a = r.array[0,0].Mult(array[0,0]).add(r.array[0,1].Mult(array[1,0]));
            b = r.array[0,1].Mult(array[0,1]).add(r.array[0,1].Mult(array[1,1]));
            c = r.array[1,0].Mult(array[0,0]).add(r.array[1,1].Mult(array[1,0]));
            d = r.array[1,0].Mult(array[0,1]).add(r.array[1,1].Mult(array[1,1]));
            ComplexMatrix newR = new ComplexMatrix(a,b, c, d);
            return newR;
        }
        public ComplexNumber Det()
        {
            ComplexNumber a = new ComplexNumber(), b = new ComplexNumber(), c = new ComplexNumber(), d = new ComplexNumber();
            ComplexNumber r = array[0, 0].Mult(array[1, 1]).Div(array[0, 1].Mult(array[1, 0]));
            return r;
        }
    }
}