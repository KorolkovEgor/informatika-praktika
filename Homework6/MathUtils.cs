﻿namespace Homework6
{
    public static class MathUtils
    {
        public static int FindGcd(int a, int b)
        {
            while (a != 0 && b != 0)
            {
                if (a > b)
                    a %= b;
                else
                    b %= a;
            }
            return a+b;
        }
    }
}