﻿using System;

namespace Homework6
{
    public class RationalFraction: IComparable
    {
        public int Numerator { get; set; }
        public int Denominator { get; set; }

        public RationalFraction(int denominator) : this(0, denominator)
        {
        }

        public RationalFraction(int numerator, int denominator)
        {
            Numerator = numerator;
            Denominator = denominator != 0
                ? denominator
                : throw new ArgumentException("Знаминатель дроби не может быть равен 0", nameof(denominator));
        }

        public RationalFraction Reduce()
        {
            var gcd = MathUtils.FindGcd(Numerator, Denominator);
            return new RationalFraction(Numerator / gcd, Denominator / gcd);
        }

        public RationalFraction add(RationalFraction b)
        {
            return (new RationalFraction(Numerator * b.Denominator + b.Numerator * Denominator,
                Denominator * b.Denominator)).Reduce();
        }

        public void add2(RationalFraction b)
        {
            var newFraction = add(b);
            (Numerator, Denominator) = (newFraction.Numerator, newFraction.Denominator);
        }

        public RationalFraction sub(RationalFraction b)
        {
            return (new RationalFraction(Numerator * b.Denominator - b.Numerator * Denominator,
                Denominator * b.Denominator)).Reduce();
        }

        public void sub2(RationalFraction b)
        {
            var newFraction = sub(b);
            (Numerator, Denominator) = (newFraction.Numerator, newFraction.Denominator);
        }

        public RationalFraction mult(RationalFraction b)
        {
            return (new RationalFraction(Numerator * b.Numerator, Denominator * b.Denominator)).Reduce();
        }

        public void mult2(RationalFraction b)
        {
            var newFraction = mult(b);
            (Numerator, Denominator) = (newFraction.Numerator, newFraction.Denominator);
        }

        public RationalFraction div(RationalFraction b)
        {
            return (new RationalFraction(Numerator * b.Denominator, Denominator * b.Numerator)).Reduce();
        }

        public void div2(RationalFraction b)
        {
            var newFraction = div(b);
            (Numerator, Denominator) = (newFraction.Numerator, newFraction.Denominator);
        }

        public override string ToString()
        {
            return $"{Numerator}/{Denominator}";
        }

        public int CompareTo(object? obj)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));
            RationalFraction p =obj as RationalFraction;
            if (p != null)
                return this.Numerator.CompareTo(p.Numerator);
            else throw new NotImplementedException();
        }
        public double Value()
        {
            return Numerator / Denominator;
        }

        public bool Equals(RationalFraction b)
        {
            return ReducedEquals(Reduce(), b.Reduce());
        }

        private static bool ReducedEquals(RationalFraction a, RationalFraction b)
        {
            return a.Numerator == b.Numerator && 
                   a.Denominator == b.Denominator;
        }

        public int NumberPart()
        {
            return Numerator / Denominator;
        }
    }
}