﻿using System;

namespace HomeworkFor6._10._2021
{
    public class Number4
    {
        public void Run()
        {
            Console.WriteLine("Введите строку");
            String n = Console.ReadLine();
            Console.WriteLine(F(n, 0));
        }

        public string F(string n,int count)
        {
            if (n.Length < 3)
            {
                return n;
            }
            else if (count < n.Length && n[count] == 'm' && n[count+1] == 'o' && n[count + 2] == 'm')
            {
                string m = "";
                for (int i = 0; i < n.Length; i++)
                {
                    if (i == count)
                    {
                        m = m + "dad";
                        i += 2;
                    }
                    else 
                        m = m + n[i];
                }
                return F(m, count + 2);
            }
            else if (count == n.Length - 1)
                return n;
            else
                return F(n, count + 1);
        }
    }
}