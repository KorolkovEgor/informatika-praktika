﻿using System;

namespace HomeworkFor6._10._2021
{
    public class Number3
    {
        public void Run()
        {
            Console.WriteLine("Введите число");
            int n = Int32.Parse(Console.ReadLine());
            int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            Console.WriteLine(F(arr,arr.Length, 0, n));
        }

        public string F(int[] array, int r, int l, int n)
        {
            if (array.Length == 1)
            {
                if (array[0] == n)
                    return "Есть";
                else
                    return "Отсутствует";
            }

            if (r - l == 1)
            {
                if (array[r-1] == n ) 
                    return "Есть";
                else if (array[l-1] == n ) 
                    return "Есть";
                else
                    return "Отсутствует";
            }
            int middle = (r + l) / 2 + (r + l) % 2;
            if (array[middle - 1] == n)
            {
                return "Есть";
            }
            else if (array[middle - 1] < n && array[middle] <= n)
                return F(array, array.Length, middle, n);
            else
                return F(array, middle, l, n);
        }
    }
}