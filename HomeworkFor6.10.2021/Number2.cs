﻿using System;

namespace HomeworkFor6._10._2021
{
    public class Number2
    {
        public void Run()
        {
            Console.WriteLine("Введите чилсо, от которого вы хотите получить факториал");
            int n = Int32.Parse(Console.ReadLine());
            Console.WriteLine(F(1, n));
        }

        public int F( int factorial,int count)
        {
            if (count > 0)
                return F(factorial * count, count - 1);
            else
                return factorial;
        }
    }
}