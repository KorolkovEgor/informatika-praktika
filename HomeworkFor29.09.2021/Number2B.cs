﻿using System;

namespace HomeworkFor29._09._2021
{
    public class Number2B
    {
        public static void SpiralArray()
        {
            var c = new int[4, 4]
            {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {10, 11, 12, 13},
                {20, 21, 22, 23}
            };

            var indentation = 0;
            var line = c.GetLength(0);
            var column = c.GetLength(1);
            var counter = line / 2 + line % 2;
            for (int i = 1; i <= counter; i++) 
            {
                //верх                
                for (var j = indentation; j < column - indentation; j++)
                    Console.Write(c[indentation, j] + " ");
                //правый                
                if (line > indentation + indentation + 2)
                {
                    for (var j = indentation + 1; j < line - indentation - 1; j++)
                        Console.Write(c[j, column - 1 - indentation] + " ");
                }

                //низ
                if (line > indentation + indentation + 1)
                {
                    for (var j = column - indentation - 1; j >= indentation; j--)
                        Console.Write(c[line - 1 - indentation, j] + " ");
                }

                //левый                
                if (line > indentation + indentation + 2)
                {
                    for (var j = line - indentation - 2; j > indentation; j--)
                        Console.Write(c[j, indentation] + " ");
                }

                indentation++;
            }
        }
    }
}