﻿namespace HomeworkFor1._12._2021
{
    abstract public class ObjectDecoration
    {
        public double Square { get; set; }
        public int SocketsQuantity { get; set; }

        protected ObjectDecoration(double square, int socketsQuantity)
        {
            Square = square;
            SocketsQuantity = socketsQuantity;
        }
    }

    public class ChristmasTree : ObjectDecoration
    {
        public ChristmasTree(double square, int socketsQuantity, double height) : base(square, socketsQuantity)
        {
            Height = height;
        }

        public double Height { get; set; }
    }

    public class ShowCase : ObjectDecoration
    {
        public ShowCase(double square, int socketsQuantity) : base(square, socketsQuantity)
        {
        }
    }
}