﻿namespace HomeworkFor1._12._2021
{
    abstract public class Decoration
    {
        public double DecorationSquare { get; set; }
        public bool SocketIsNeeded { get; set; }

        protected Decoration(double decorationSquare, bool socketIsNeeded)
        {
            DecorationSquare = decorationSquare;
            SocketIsNeeded = socketIsNeeded;
        }
    }

    public class Garland : Decoration
    {
        public int ColorsQuantity { get; set; }
        public int ModesQuantity { get; set; }

        public Garland(double decorationSquare, bool socketIsNeeded, int colorsQuantity, int modesQuantity) : base(decorationSquare, socketIsNeeded)
        {
            ColorsQuantity = colorsQuantity;
            ModesQuantity = modesQuantity;
        }
    }

    public class Toy : Decoration
    {
        public double Weight { get; set; }
        public double Fragility { get; set; }

        public Toy(double decorationSquare, bool socketIsNeeded, double weight, double fragility) : base(decorationSquare, socketIsNeeded)
        {
            Weight = weight;
            Fragility = fragility;
        }
    }
}