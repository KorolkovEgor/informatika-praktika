﻿using System;

namespace HomeworkFor1._12._2021
{
    class Program
    {
        static void Main(string[] args)
        {
            var t1 = new ChristmasTree(26.2, 2, 14);
            var Garland = new Garland(8, true, 15, 5);
            var Toy = new Toy(2, false, 20, 12);
            static void DecorateTree(ObjectDecoration first,Garland garland, Toy toy)
            {
                int GarlandsQuantity = 0;
                int ToysQuantity = 0;
                while (first.SocketsQuantity>0)
                {
                    first.SocketsQuantity -= 1;
                    GarlandsQuantity += 1;
                    first.Square -= GarlandsQuantity * garland.DecorationSquare;
                }

                while (first.Square>0)
                {
                    first.Square -= toy.DecorationSquare;
                    ToysQuantity += 1;
                }
                Console.WriteLine($"Вы повесили {GarlandsQuantity} гирлянд");
                Console.WriteLine($"Вы повесили {ToysQuantity} игрушек");
            }
        }
    }
}