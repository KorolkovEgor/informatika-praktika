﻿using System;

namespace Triangle
{
    public class Triangle
    {
        public double Angle1 { get; set; }
        public double Angle2 { get; set; }
        public double Angle3 { get; set; }
        public double Side1 { get; set; }
        public double Side2{ get; set; }
        public double Side3 { get; set; }
        /// <summary>
        /// Метод вычисления периметра
        /// </summary>
        /// <param name="Side1">Сторона 1</param>
        /// <param name="Side2">Сторона 2</param>
        /// <param name="Side3">Сторона 3</param>
        public void Perimeterer(double Side1, double Side2, double Side3)
        {
            var perimeter = Side1 + Side2 + Side3;
            Console.WriteLine(perimeter);
        }
        /// <summary>
        /// етод вычисления площади
        /// </summary>
        /// <param name="Side1">сторона 1</param>
        /// <param name="Side2">сторона 2</param>
        /// <param name="Side3">сторона 3</param>
        /// <param name="perimeter">переменная периметр</param>
        public void Square(double Side1, double Side2, double Side3,double perimeter)
        {
            var Square = Math.Sqrt(perimeter * (perimeter - Side1) * (perimeter - Side2) * (perimeter - Side3));
        }
    }
}