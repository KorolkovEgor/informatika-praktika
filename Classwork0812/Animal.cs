﻿using System;

namespace Classwork0812
{
    public class Animal
    {
        public struct Aeroflot
        {
            public string Destination; //место назначения 
            public int Number; //номер номер рейса 
            public PlaneType Planetype;//тип самолета

            public Aeroflot(string destination, int number, PlaneType planetype)
            {
                Destination = destination;
                Number = number;
                Planetype = planetype;
            }
        }

        public enum PlaneType
        {
            Boeing,Il
        }
    }
}