﻿using System;

namespace Classwork0812
{
    public class Musicians:IPLayer
    {
        static string Name { get; set; }
        public string Instrument { get; set; }

        public Musicians(string name, string instrument)
        {
            Name = name;
            Instrument = instrument;
        }

        public void Play()
        {
            if (Instrument == "Скрипка")
            {
                Console.WriteLine($"Музыкант  {Name} играет на Скрипке");
            }
        }
    }
}