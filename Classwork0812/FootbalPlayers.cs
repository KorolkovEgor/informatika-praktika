﻿using System;

namespace Classwork0812
{
    public class FootbalPlayers:IPLayer
    {
        public int Age { get; set; }
        public string Name { get; set; }

        public FootbalPlayers(int age, string name)
        {
            Age = age;
            Name = name;
        }

        public void Play()
        {
            if (Age < 20)
            {
                Console.WriteLine($"Футболист {Name} играет на поле");
            }
        }
    }
}