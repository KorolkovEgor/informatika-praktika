﻿using System;
using System.IO;
namespace Homework22._12._2021
{
    public class Number4
    {
        public void Task4 ()
        { 
            File. WriteAllText(@"D:\Тест1.txt", $"{DateTime.Now}");
            File.WriteAllText(@"D:\Тест2.txt", $"{DateTime.Now}");
            File.WriteAllText(@"D:\Тест3.txt", $"{DateTime.Now}");
            var array = Directory.GetFiles(@"D:\");
            var maximum = array[0];
            foreach (var member in array) { if (File.GetLastWriteTime(maximum) < File.GetLastWriteTime(member)) { maximum = member; }}
            Console.WriteLine(File.GetLastWriteTime(maximum));
        }
    }
}