﻿using System;

namespace Homework22._12._2021
{
    public class Train:IComparable
    {
        public string Destination;
        public int TrainNumber;
        public string DepartureTime;

        public Train(string destination, int trainNumber, string departureTime)
        {
            Destination = destination;
            TrainNumber = trainNumber;
            DepartureTime = departureTime;
        }
        public static Train[] Fulling()
        {
            Console.WriteLine("Введодите данные о каждом рейсе");
            Train[] list = new Train[8];
            for (int i = 0; i < 8; i++) //введение данных о рейсах
            {
                var strings = Console.ReadLine().Split(' ');
                list[i] = new Train
                    (strings[0],Checking(strings[1]), strings[2]);
            
            }
            Console.WriteLine(list.Length);
            return list;
        }
        static int Checking(string str) //проверка чисел 
        {
            int number = 0;
            while (!Int32.TryParse(str,out number))
            {
                Console.WriteLine("Вы ввели некорректноне значение, введите новое");
                str=Console.ReadLine();
            }
            return number;
        }
        public int CompareTo(object? obj)
            {
                var obje =(Train) obj;
                if (obj==null) return -1;
                if (obj!=null)
                {
                    if (obje.TrainNumber < this.TrainNumber) return 1;
                    else if (obje.TrainNumber > this.TrainNumber) return -1;
                    else return 0;
                }

                throw new Exception("Unnable to compare objects");
            }
        public static void Print(Train member)
        {
            Console.WriteLine($"Поезд номер {member.TrainNumber} отправляется в {member.Destination} в {member.DepartureTime}");
        }
        public static void Print(Train[] list)
        {
            foreach (Train member in list) Print(member);
        }

        public static void Displaying(Train[] list, string destination)
        {
            int counter = 0;
            foreach (Train member in list)
            {
                if (member.TrainNumber==Checking(destination))
                {
                    Print(member);
                    counter++;
                }
            }

            if (counter == 0) Console.WriteLine("Нет рейсов в это время");
        }
        
    }
}