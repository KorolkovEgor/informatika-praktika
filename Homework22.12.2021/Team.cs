﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Homework22._12._2021
{
    public struct Team:IComparable
    {
        public string Name;
        public DateTime Date;
        private int points;
        public int Points
        {
            get
            {
                return points;
            }
            set
            {
                if (value>=0)
                {
                    points = value;
                }
            }
        }
        public Team(string name, DateTime date, int points) : this()
        {
            Name = name;
            Date = date;
            Points = points;
        }
        public static Team[] ReadFromFile(string path)
        {
            string []lines=File.ReadAllLines(path);
            List<Team> list = new List<Team>();
            foreach (var line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;
                var currentline = line.Split(' ');
                if (currentline.Length!=3) continue;
                var date = currentline[1].Split('.');
                list.Add(new Team(currentline[0],new DateTime(ToInt(date[0]),ToInt(date[1]),ToInt(date[2]),ToInt(date[3]),ToInt(date[4]),0),ToInt(currentline[2])));
                int ToInt(string line)
                {
                    return Int32.Parse(line);
                }
            }
            return list.ToArray();
        }

        public static void DisplayWinner(DateTime date1,DateTime date2,Team[] array)
        {
            
            // var list=array.ToList();
            // if (date1>date2)
            // {
            //     (date1, date2) = (date2, date1);
            // }
            // for (int i = 0; i < list.Count; i++)
            // {
            //     if (list[i].Date<date1  || list[i].Date>date2)
            //     {
            //         list.Remove(list[i]);
            //     }
            // }

        }
        

        

        public int CompareTo(object? obj)
        {
            var obje =(Team) obj;
            if (obj==null) return -1;
            if (obj!=null)
            {
                if (obje.Points< this.Points) return -1;
                else if (obje.Points > this.Points) return 1;
                else return 0;
            }

            throw new Exception("Unnable to compare objects");
        }
    }
}