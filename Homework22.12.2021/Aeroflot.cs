﻿using System;
using System.Collections.Generic;

namespace Homework22._12._2021
{
    public struct Aeroflot : IComparable

    {
    public string Destination;
    public int FlightNumber;
    public string PlaneType;

    public static Aeroflot[] Fulling()
    {
        Console.WriteLine("Введодите данные о каждом рейсе");
        Aeroflot[] list = new Aeroflot[7];
        for (int i = 0; i < 7; i++) //введение данных о рейсах
        {
            var strings = Console.ReadLine().Split(' ');
            list[i] = new Aeroflot
                (strings[0],Checking(strings[1]), strings[2]);
            
        }
        Console.WriteLine(list.Length);
        return list;
    }


    static int Checking(string str) //проверка чисел 
    {
        int number = 0;
        while (!Int32.TryParse(str,out number))
        {
            Console.WriteLine("Вы ввели некорректноне значение, введите новое");
            str=Console.ReadLine();
        }
        return number;
    }

    public Aeroflot(string destination, int flightNumber, string planeType)
    {
        Destination = destination;
        FlightNumber = flightNumber;
        PlaneType = planeType;
    }

    public int CompareTo(object? obj)
    {
        var obje =(Aeroflot) obj;
        if (obj==null) return -1;
        if (obj!=null)
        {
            if (obje.FlightNumber < this.FlightNumber) return 1;
            else if (obje.FlightNumber > this.FlightNumber) return -1;
            else return 0;
        }

        throw new Exception("Unnable to compare objects");
    }

    public static void Print(Aeroflot member)
    {
        Console.WriteLine($"Рейс номер {member.FlightNumber}, летящий на самолете {member.PlaneType}");
    }

    public static void Print(Aeroflot[] list)
    {
        foreach (Aeroflot member in list) Print(member);
    }

    public static void Displaying(Aeroflot[] list, string destination)
    {
        int counter = 0;
        foreach (Aeroflot member in list)
        {
            if (member.Destination==destination)
            {
                Print(member);
                counter++;
            }
        }

        if (counter == 0) Console.WriteLine("Нет рейсов в данном направлении");
    }
    }
}