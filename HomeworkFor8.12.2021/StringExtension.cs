﻿namespace HomeworkFor8._12._2021
{
    public static class StringExtension
    {
        public static string Addition(this string string1, string string2)
        {
            return string.IsNullOrWhiteSpace(string1)|| string.IsNullOrWhiteSpace(string2)
                ? string.IsNullOrWhiteSpace(string1) ? string2 : string1
                : CompileString(string1, string2);
        }

        private static string CompileString(string string1, string string2)
        {
            var line1 = string1.Split(' ');
            var line2 = string2.Split(' ');
            var newline = new string[line1.Length + line2.Length];
            for (int i = 0; i < newline.Length; i++)
            {
                var projectedIndex = i / 2;
                if (line1.Length <= projectedIndex || line2.Length <= projectedIndex)
                {
                    newline[i] = line1.Length <= projectedIndex ? line2[i-line1.Length] : line1[i-line2.Length];
                }
                else
                {
                    newline[i] = i % 2 == 0 ? line1[projectedIndex] : line2[projectedIndex];
                }
            }

            return string.Join(" ", newline);
        }
    }
}