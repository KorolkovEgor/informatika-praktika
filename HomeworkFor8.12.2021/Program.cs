﻿using System;

namespace HomeworkFor8._12._2021
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1 2 3 4".Addition("5 6 7 8"));
            Console.WriteLine("1 2 3".Addition("5 6 7"));
            Console.WriteLine("1 2 3 4".Addition("5 6 7"));
            Console.WriteLine("1 2 3".Addition("5 6 7 8"));
            Console.WriteLine("1".Addition("5 6 7 8"));
            Console.WriteLine("1".Addition("5"));
            Console.WriteLine("1 2 3 4 5".Addition("5"));
            Console.WriteLine("".Addition("5"));
            Console.WriteLine("1".Addition(""));
            Console.WriteLine("1 2 3".Addition(null));
        }
    }
}