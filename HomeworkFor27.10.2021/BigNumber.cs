﻿using System;

namespace HomeworkFor27._10._2021
{
    class BigNumber
    {
        private int[] arr;
        public string s;

        public BigNumber(int a)
        {
            s = Convert.ToString(a);
            arr = ToArray(s);
        }
        public int[] Add(BigNumber number2)
        {
            if (arr.Length < number2.s.Length)
            {
                s = AdditionZero(s, number2.s);
                arr = AddZero(arr, number2.arr);
            }
            else number2.s = AdditionZero(number2.s, s);

            int[] array = Addition(arr, number2.arr);

            return array;
        }
        static int[] ToArray(string s)
        {
            int[] array = new int[s.Length];
            for (int i = 0; i < s.Length; i++)
            {
                array[i] = s[i] - '0';
            }
            return array;
        }
        public int[] Addition(int[] arr1, int[] arr2)
        {
            int[] array = new int[arr1.Length];
            for (int i = arr1.Length - 1; i >= 0; i--)
            {
                array[i] = arr1[i] + arr2[i];
                if (array[i] > 9)
                {
                    arr1[i - 1] = arr1[i - 1] + array[i] / 10;
                    array[i] = array[i] % 10;
                }
            }
            return array;
        }
        public string AdditionZero(string s1, string s2)
        {
            int zero = s2.Length - s1.Length;
            for (int i = 0; i < zero; i++)
                s1 = "0" + s1;
            return s1;
        }

        public bool Compare(BigNumber n1, BigNumber n2)
        {
            bool b = false;
            for (int i = 0; i < n1.arr.Length; i++)
            {
                b = n1.arr[i] < n2.arr[i];
                if (b == true)
                    break;
            }
            return b;
        }
        public int[] Sub(BigNumber number1, BigNumber number2)
        {
            if (number1.s.Length < number2.s.Length || Compare(number1, number2))
            {
                int[] arr = number1.arr;
                number1.arr = number2.arr;
                number2.arr = arr;

                string s0 = number1.s;
                number1.s = number2.s;
                number2.s = s0;
            }

            if (number1.s.Length < number2.s.Length)
                number1.s = AdditionZero(number1.s, number2.s);
            else
            {
                number2.s = AdditionZero(number2.s, number1.s);
                number2.arr = AddZero(number2.arr, number1.arr);
            }

            return Subtracting(number1.arr, number2.arr);
        }
        public int[] Subtracting(int[] arr1, int[] arr2)
        {
            int[] array = new int[arr1.Length];
            for (int i = arr1.Length - 1; i >= 0; i--)
            {
                if (arr1[i] < arr2[i])
                {
                    arr1[i] = arr1[i] + 10;
                    arr1[i - 1]--;
                }
                array[i] = arr1[i] - arr2[i];
            }
            return array;
        }
        public int[] Multi(BigNumber n2)
        {
            if (s.Length <= n2.s.Length)
            {
                string dop = s;
                s = n2.s;
                n2.s = dop;
            }
            int[] array = Multiplication(arr, n2.arr);
            return array;
        }
        public int[] Multiplication(int[] arr1, int[] arr2)
        {
            int k = 0;
            int[] myArray = new int[arr1.Length + arr2.Length];
            for (int i = arr2.Length - 1; i >= 0; i--)
            {
                int[] arr = new int[arr1.Length + k];
                int[] a = Multiplic(arr1, arr2[i]);
                for (int j = 0; j < a.Length; j++)
                    arr[j] = a[j];
                myArray = Adding(arr, myArray);
                k++;
            }
            return myArray;
        }
        public int[] Multiplic(int[] arr1, int n)
        {
            int[] dop = new int[arr1.Length - 1];
            int[] array = new int[arr1.Length];
            for (int i = arr1.Length - 1; i >= 0; i--)
            {
                if (i == arr1.Length - 1)
                {
                    array[i] = (arr1[i] * n) % 10;
                    dop[i - 1] = arr1[i] * n / 10;
                }
                else if (i == 0)
                    array[i] = (arr1[i] * n + dop[i]);
                else
                {
                    array[i] = (arr1[i] * n + dop[i]) % 10;
                    dop[i - 1] = (arr1[i] * n + dop[i]) / 10;
                }
            }
            return array;
        }
        public int[] AddZero(int[] a1, int[] a2)
        {
            int zero = a2.Length - a1.Length;
            int[] arr3 = new int[a2.Length];
            for (int i = 0; i < zero; i++)
                arr3[i] = 0;
            int k = 0;
            for (int i = zero; i < a2.Length; i++)
            {
                arr3[i] = a1[k];
                k++;
            }
            return arr3;
        }
        public int[] Adding(int[] arr1, int[] arr2)
        {
            int[] array = new int[Math.Max(arr1.Length, arr2.Length)];
            if (arr1.Length < array.Length)
                arr1 = AddZero(arr1, array);
            else array = AddZero(array, arr1);

            for (int i = array.Length - 1; i >= 0; i--)
            {
                array[i] = arr1[i] + arr2[i];
                if (array[i] > 9)
                {
                    if (i == 0)
                        array[i] = array[i];
                    else
                    {
                        arr1[i - 1] = arr1[i - 1] + array[i] / 10;
                        array[i] = array[i] % 10;
                    }
                }
            }
            return array;
        }
        public string NegativeNumber(BigNumber n1, BigNumber n2)
        {
            string z = " ";
            if (s.Length < n2.s.Length || Compare(n1, n2))
                z = "-";
            return z;
        }
    }
}
