﻿namespace Controlnaya15._12._2021
{
    public abstract class Appliances
    {
        public string Manufaacturer { get; set; }//Сеттер установлен для реализации идеи переопределения метода Equals
        public int Power { get; set; }//Сеттер установлен для реализации идеи переопределения метода Equals
        public string EnergyClass { get; set; }//Сеттер установлен для реализации идеи переопределения метода Equals
        public string Name { get; }//имя не может быть заменено

        protected Appliances(string manufaacturer, int power, string energyClass, string name)//конструктор абстрактного класса
        {
            Manufaacturer = manufaacturer;
            Power = power;
            EnergyClass = energyClass;
            Name = name;
        }
    }
}