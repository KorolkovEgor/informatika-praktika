﻿using System;
using System.Dynamic;

namespace Controlnaya15._12._2021
{
    public class Queue
    {
        public KitchenAppliances[] KitchenArray { get; set; }//реализую очередь через массив
        private int index;//номер элемента в массиве, для первого элемента равен 0

        public Queue(KitchenAppliances[] array)//очередь с элементами
        {
            KitchenArray = array;
            index = array.Length;
        }

        public Queue()//пустая очередь
        {
            KitchenArray = new KitchenAppliances[0];
            index = 0;
        }
        /// <summary>
        /// Массив увеличивается на один и этот элемент заменяется на новый
        /// </summary>Старый массив
        /// <param name="added">Входной массив кухонной техники</param>
        public void Add( KitchenAppliances added)
        {
            KitchenAppliances[] newArray = new KitchenAppliances[index + 1];
            for (int i = 0; i < index; i++)
            {
                newArray[i] = KitchenArray[i];
            }

            newArray[index] = added;
            KitchenArray = newArray;
            index++;
        }
        /// <summary>
        /// Метод чтения вытаскивает элемент с нулевым индексом и уменьшает массив на 1
        /// </summary>На вход ничего
        /// <returns>нулевой элемент массива</returns>
        public KitchenAppliances Read()
        {
            if (KitchenArray.Length==0)
            {
                Console.WriteLine("Элемент не может быть выведен,так как очердь пуста");
                return null;
            }
            else
            {
                KitchenAppliances old = KitchenArray[0];
                KitchenAppliances[] newArray = new KitchenAppliances[index - 1];
                for (int i = 1; i <= index - 1; i++)
                {
                    newArray[i - 1] = KitchenArray[i];
                }

                KitchenArray = newArray;
                index--;
                return old;
            }
        }
        /// <summary>
        /// Метод выведения очереди,котовый выводит название техники находится в очереди
        /// </summary>
        /// <param name="queue">Массив техники</param>
        public void QueueInfo()
        {
            if (KitchenArray.Length==0)
            {
                Console.WriteLine("Очередь пуста");
            }

            foreach (KitchenAppliances member in KitchenArray)
            {
                if (member != null)
                {
                    Console.WriteLine(member.Name+' ');
                }
            }
        }

    }
}