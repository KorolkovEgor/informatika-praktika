﻿namespace Controlnaya15._12._2021
{
    public static class KitchenExtension
    {
        /// <summary>
        /// етод расширения 
        /// </summary>
        /// <param name="tech">объект кухонной техники</param>
        /// <returns>возвращает время работы</returns>
        public static int Fixing( this KitchenAppliances tech)
        {
            return tech.Power / 10;
        }
    }
}