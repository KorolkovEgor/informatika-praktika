﻿using System;
using System.Dynamic;

namespace Controlnaya15._12._2021
{
    public class KitchenAppliances:Appliances,ICloneable
    {
        public bool BuiltIn { get; set; }

        public KitchenAppliances(string manufaacturer, int power, string energyClass, string name, bool builtIn) : base(manufaacturer, power, energyClass, name)
        {
            BuiltIn = builtIn;
        }
        /// <summary>
        /// метод встраивания,который проверяет встраиваемость объекта и выводит соответствующее сообщение
        /// </summary>сообщение о возможности встраивания техники
        /// <param name="first"></param>
        public void SettingUp(KitchenAppliances first)
        {
            if (first.BuiltIn == true)
            {
                Console.WriteLine("Техника встроена");
            }
            else
            {
                Console.WriteLine("Техника не может быть встроена");
            }
        }
        public override int GetHashCode()//переопределение метода GetHashCode по мощности и производителю
        {
            int hashcode = Power;
            if (Manufaacturer == "КФУ")
            {
                hashcode += 1;
            }
            else
            {
                hashcode -= 1;
            }

            return hashcode;
        }

        public override bool Equals(object? obj)//переопределение метода Equals по производителю мощности и классу энергопотребления
        {
            if ((obj as KitchenAppliances).Manufaacturer == Manufaacturer
                &&(obj as KitchenAppliances).Power==Power
                &&(obj as KitchenAppliances).EnergyClass==EnergyClass)
            {
                return Equals(obj as KitchenAppliances);
            }
            else
            {
                return false;
            }
        }

        public override string ToString()//переопределение класса ToString для выведения информации об устройстве 
        {
            return ($"{Name} произведен(а) на заводе {Manufaacturer} и имеет мощность{Power} и {EnergyClass} класс энергопотребления");
        }

        public object Clone()
        {
            return new KitchenAppliances(Manufaacturer, Power, EnergyClass, Name,BuiltIn);
        }
    }
}