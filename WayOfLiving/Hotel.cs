﻿namespace WayOfLiving
{
    public class Hotel:WayOfLiving
    {
        public int RoomNumber { get; set; }//Номер комнаты
        public RoomType Room { get; set; }//Тип комнаты
        public FeedingType Feeding { get; set; }//Тип питания
    }
    /// <summary>
    /// Перечисления типов комнат 
    /// </summary>
    public enum RoomType
    {
        SoloRoom,
        DuoRoom,
        FamilyRoom,
    }
    /// <summary>
    /// Типы питания
    /// </summary>
    public enum FeedingType
    {
        BreakFastOnly,
        Foolday
    }
}