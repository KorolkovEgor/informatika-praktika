﻿using System;

namespace WayOfLiving
{
    public class WayOfLiving
    {
        public double Price { get; set; }//Цена аренды
        public int MinRentTimeInDays { get; set; }//минимальный срок аренды
        public int MaxRentTimeInDays { get; set; }//максимальный срок аренды
        public bool PrepaidExpense { get; set; }//нужен ли залог
        /// <summary>
        /// Метод заселения
        /// </summary>
        public void Settle()
        {
            Console.WriteLine("Вы заселены");
        }
        /// <summary>
        /// метод выселения
        /// </summary>
        public void Eject()
        {
            Console.WriteLine("Вы выселены");
        }
    }
}