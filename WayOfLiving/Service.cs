﻿namespace WayOfLiving
{
    public class Service:WayOfLiving
    {
        public bool CleaningPaid { get; set; }//Оплачена ли уборка
        public bool SecondKeyComplectPaid { get; set; }//Оплачен ли второй комплект ключей
        public bool InternetPaid { get; set; }//Оплачен ли интернет
        public RentType Space { get; set; }//Тип аренды
        
    }
    /// <summary>
    /// Перечисление типов аренды
    /// </summary>
    public enum RentType
    {
        FoolFlat,
        RoomOnly
    }
}