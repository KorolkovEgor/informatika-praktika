﻿using System;

namespace HomeworkFor15._09._2021
{
    class Program
    {
        static void Main(string[] args)
        {
            static void Main(string[] args)
            {
                Console.WriteLine("Введите высоту ёлки");
                int height = Int32.Parse(Console.ReadLine());
                int count = 1;
                int n, j, i;
                
                for (int lines = height; lines >= 1; lines--)// одинарная пирамида 
                {
                    for (int l = 1; l <= height * 2 - 1; l++)
                    {
                        Console.Write(" ");
                    }

                    for (int spaces = lines - 1; spaces >= 1; spaces--)
                    {
                        Console.Write(" ");
                    }

                    for (int star = 1; star <= count; star++)
                    {
                        Console.Write("*");
                        Console.Write(" ");
                    }

                    count++;
                    Console.WriteLine();
                }

                for (i = 1; i <= height; i++)
                {
                    for (j = 1; j <= (height - i + height - 1); j++) // отступы второй пирамиды
                    {
                        Console.Write(" ");
                    }

                    for (n = 1; n <= (i * 2 - 1); n++) // 2 пирамида
                    {
                        Console.Write("*");
                    }

                    for (int m = height * 2 - n + 1; m >= 1; m--) // пробел между второй и третьей 
                    {
                        Console.Write(" ");
                    }

                    for (n = 1; n <= (i * 2 - 1); n++) // 3 пирамида
                    {
                        Console.Write("*");
                    }

                    Console.WriteLine();

                }
            }
        }
    }
}